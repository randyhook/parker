from signal import signal, SIGTERM, SIGHUP, pause
from threading import Thread
from time import sleep
from gpiozero import LED, DistanceSensor

red_light = LED(4)
green_light = LED(22)

red_light.off()
green_light.off()

sonar = DistanceSensor(max_distance=3, trigger=17, echo=27)
reading = True

def safe_exit(signum, frame):
    exit()

def park_stop():
    red_light.on()
    green_light.off()

def park_slow():
    red_light.off()
    green_light.blink(on_time=0.25, off_time=0.25)

def park_go():
    red_light.off()
    green_light.on()

def read_distance():
    park_state = "unknown"

    while reading:
        print(sonar.distance)

        if sonar.distance <= 0.5:
            if not park_state == "stop":
                park_stop()
                park_state = "stop"
        elif sonar.distance <= 1.1:
            if not park_state == "slow":
                park_slow()
                park_state = "slow"
        else:
            if not park_state == "go":
                park_go()
                park_state = "go"

        sleep(0.1)

signal(SIGTERM, safe_exit)
signal(SIGHUP, safe_exit)

try:
    reader = Thread(target=read_distance, daemon=True)
    reader.start()

    pause()

except KeyboardInterrupt:
    pass

finally:
    reading = False
    sonar.close()

    red_light.on()
    green_light.off()

    print()



